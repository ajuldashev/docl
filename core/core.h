#ifndef CORE
#define CORE
#include "../config.h"
#include "memory/memory.h"
#include "type/type.h"

#ifdef DEBUG_CORE
void core_test();
#endif

#endif
