#include "core.h"

#ifdef DEBUG_CORE
void core_test(){
	char com[256];
	puts("Hi, this is test the core");
	printf(">> ");
	scanf("%s", com);
	while (strcmp(com, "stop") != 0){
		if (strcmp(com, "help") == 0){
			puts("1) help");
			#ifdef DEBUG_MEMORY
			puts("2) memory");
			#endif
			puts("3) type");
			puts("4) stop");
		}
		#ifdef DEBUG_MEMORY
		if (strcmp(com, "memory") == 0){
			memory_test();
		}
		#endif
		printf(">> ");
		scanf("%s", com);
	}
	puts("Testing finished ;)");
}
#endif
