#ifndef CORE_TYPE
#define CORE_TYPE
#include "../core.h"

#define  null ((void*)0)

typedef enum{false = 0, true = 1} bool;

typedef char		s8int;
typedef unsigned char	u8int;

typedef short		s16int;
typedef unsigned short	u16int;

typedef int		s32int;
typedef unsigned int	u32int;

typedef long		s64int;
typedef unsigned long	u64int;

#endif
