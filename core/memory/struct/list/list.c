#include "list.h"

void mem_list_init(struct mem_list_st * list){
	list->beg = 0;
	list->end = 0;
	list->cur = 0;
	list->pos_cur = -1;
	list->length = 0;
}

void mem_list_insert(struct mem_list_st * list, void * mem, int pos){
	if (list == 0 || mem == 0){
		return;
	}
	if (list->length == 0){
		list->beg = mem_alloc(sizeof(struct mem_list_node_st));
		if (list->beg == 0){
			return;
		}
		list->beg->mem = mem;
		list->beg->next = 0;
		list->beg->prev = 0;
		list->end = list->beg;
		list->cur = list->beg;
		list->pos_cur = 0;
		list->length = 1;
		return;
	}
	if (list->length == 1 && pos > 0){
		list->end = mem_alloc(sizeof(struct mem_list_node_st));
		if (list->end == 0){
			list->end = list->beg;
			return;
		}
		list->beg->next = list->end;
		list->end->mem = mem;
		list->end->next = 0;
		list->end->prev = list->beg;
		list->cur = list->end;
		list->pos_cur = 1;
		list->length = 2;
		return;
	}
	mem_list_at(list,pos);
	struct mem_list_node_st * temp = mem_alloc(sizeof(struct mem_list_node_st));
	if (temp == 0){
		return;
	}
	if (list->cur == list->beg){
		temp->next = list->beg;
		temp->prev = 0;
		temp->mem = mem;
		list->beg->prev = temp;
		list->beg = temp;
		list->length += 1;
		list->pos_cur += 1;
		return;
	}
	if (list->cur == list->end && pos >= list->length){
		temp->next = 0;
		temp->prev = list->end;
		temp->mem = mem;
		list->end->next = temp;
		list->end = temp;
		list->length += 1;
		return;
	}
	temp->next = list->cur;
	temp->prev = list->cur->prev;
	temp->mem = mem;
	list->cur->prev->next = temp;
	list->cur->prev = temp;
	list->pos_cur += 1;
	list->length += 1;	
}


void mem_list_push_beg(struct mem_list_st * list, void * mem){
	mem_list_insert(list,mem,0);
}

void mem_list_push_end(struct mem_list_st * list, void * mem){
	mem_list_insert(list,mem,list->length);
}

void * mem_list_pop_beg(struct mem_list_st * list){
	return mem_list_get(list,0);
}

void * mem_list_pop_end(struct mem_list_st * list){
	return mem_list_get(list,list->length);
}

void * mem_list_at(struct mem_list_st * list, int pos){
	if (list == 0){
		return 0;
	}
	if (pos <= 0){
		list->cur = list->beg;
		list->pos_cur = 0;
		return list->beg->mem;
	}
	if (pos >= list->length){
		list->cur = list->end;
		list->pos_cur = list->length - 1;
		return list->end->mem;
	}
	if (pos == list->pos_cur){
		return list->cur->mem;
	}
	if (pos < list->pos_cur){
		// deg up
		int du = pos;
		// cur down
		int cd = list->pos_cur - pos;
		if (du < cd) {
			list->cur = list->beg;
			list->pos_cur = 0;
			for (; list->pos_cur < pos; list->pos_cur += 1, list->cur = list->cur->next);
			return list->cur->mem;
		} else {
			for (; list->pos_cur > pos; list->pos_cur -= 1, list->cur = list->cur->prev);
			return list->cur->mem;
		}
	}
	// cur up
	int cu = pos - list->pos_cur;
	// end down
	int ed = list->length - pos;
	if (ed < cu) {
		list->cur = list->end;
		list->pos_cur = list->length - 1;
		for (; list->pos_cur > pos; list->pos_cur -= 1, list->cur = list->cur->prev);
		return list->cur->mem;
	} else {
		for (; list->pos_cur < pos; list->pos_cur += 1, list->cur = list->cur->next);
		return list->cur->mem;
	}
	return 0;
}

void * mem_list_get(struct mem_list_st * list, int pos){
	if (list == 0){
		return 0;
	}
	if (list->length == 0){
		return 0;
	}
	if (list->length == 1){
		void * mem = list->beg->mem;
		mem_free(list->beg);
		list->beg = 0;
		list->end = 0;
		list->cur = 0;
		list->length = 0;
		list->pos_cur = -1;
		return mem;
	}
	if (pos <= 0){
		void * mem = list->beg->mem;
		list->cur = list->beg->next;
		list->pos_cur = 0;
		list->length -= 1;
		mem_free(list->beg);
		list->beg = list->cur;
		list->cur->prev = 0;
		return mem;

	}
	if (pos >= list->length - 1){
		void * mem = list->end->mem;
		list->cur = list->end->prev;
		list->length -= 1;
		list->pos_cur = list->length - 1;
		mem_free(list->end);
		list->end = list->cur;
		list->end->next = 0;
		return mem;

	}
	mem_list_at(list,pos);
	struct mem_list_node_st * node = list->cur;
	list->cur->prev->next = list->cur->next;
	list->cur->next->prev = list->cur->prev;
	void * mem = list->cur->mem;
	list->cur = list->cur->next;
	list->length -= 1;
	mem_free(node);
	return mem;
}

void mem_list_goto_beg(struct mem_list_st * list){
	mem_list_at(list,0);
}

void mem_list_goto_end(struct mem_list_st * list){
	mem_list_at(list,list->length);
}

void mem_list_next(struct mem_list_st * list){
	mem_list_at(list, list->pos_cur + 1);
}

void mem_list_prev(struct mem_list_st * list){
	mem_list_at(list, list->pos_cur - 1);
}

void mem_list_free(struct mem_list_st * list){
	if (list == 0) {
		return;
	}
	while (list->length > 0){
		mem_free(mem_list_pop_beg(list));
	}
}

#ifdef DEBUG

void mem_list_test(){
	struct mem_list_st list;
	mem_list_init(&list);

	char com[1024];
	puts("Hello this is list test ;)");
	printf(">> ");
	scanf("%s",com);
	while (strcmp(com,"stop") != 0){
		if (strcmp(com, "help") == 0){
			puts("*)");
			puts(" 1) help   	-> Show this massage");
			puts(" 2) print  	-> Print list");
			puts(" 3) insert 	-> insert int in list");
			puts(" 4) push_beg 	-> push in begin the list");
			puts(" 5) push_end  	-> push in end the list");
			puts(" 6) at 	   	-> get element from index");
			puts(" 7) get		-> get element from index and delet");
			puts(" 8) pop_beg 	-> get element from begin list and delet");
			puts(" 9) pop_end	-> get element from end list and delete");
			puts("10) goto_beg 	-> goto begin in the list current pointer");
			puts("11) goto_end 	-> goto end in the list current pointer");
			puts("12) free		-> free the lits");
			puts("13) status 	-> show status the structure mem_list_st");
			puts("*)");
		}
		if (strcmp(com,"print") == 0){
			int i = 0;
			for (; i < list.length; ++i){
				mem_list_at(&list,i);
				printf("%5i -> %10i\n", i, *((int*)(list.cur->mem)));
			}
		}
		if (strcmp(com,"insert") == 0){
			int ch = 0;
			int pos = 0;
			scanf("%i",&ch);
			scanf("%i",&pos);
			int * mem = mem_alloc(sizeof(int));
			*mem = ch;
			mem_list_insert(&list,mem,pos);
		}
		if (strcmp(com,"push_beg") == 0){
			int ch = 0;
			int pos = 0;
			scanf("%i",&ch);
			int * mem = mem_alloc(sizeof(int));
			*mem = ch;
			mem_list_push_beg(&list,mem);
		}
		if (strcmp(com,"push_end") == 0){
			int ch = 0;
			int pos = 0;
			scanf("%i",&ch);
			int * mem = mem_alloc(sizeof(int));
			*mem = ch;
			mem_list_push_end(&list,mem);
		}
		if (strcmp(com,"at") == 0){
			int pos = 0;
			scanf("%i",&pos);
			mem_list_at(&list,pos);
			printf("%i\n",*((int*)list.cur->mem));
		}
		if (strcmp(com,"get") == 0){
			int pos = 0;
			scanf("%i",&pos);
			int * num = ((int*)mem_list_get(&list,pos));
			if (num != 0) {
				printf("%i\n",*num);
				mem_free(num);
			} else {
				puts("list free");
			}
		}
		if (strcmp(com,"pop_beg") == 0){
			int * num = ((int*)mem_list_pop_beg(&list));
			if (num != 0) {
				printf("%i\n",*num);
				mem_free(num);
			} else {
				puts("list free");
			}
		}
		if (strcmp(com,"pop_end") == 0){
			int * num = ((int*)mem_list_pop_end(&list));
			if (num != 0) {
				printf("%i\n",*num);
				mem_free(num);
			} else {
				puts("list free");
			}
		}
		if (strcmp(com,"goto_beg") == 0){
			mem_list_goto_beg(&list);
		}
		if (strcmp(com,"goto_end") == 0){
			mem_list_goto_end(&list);
		}
		if (strcmp(com,"free") == 0){
			mem_list_free(&list);
		}
		if (strcmp(com, "status") == 0){
			if (list.beg == 0){
				printf("begin   : %10i\n",list.beg);
			} else {
				printf("begin   : %10i next : %10i prev : %10i mem : %10i\n",list.beg, list.beg->next, list.beg->prev, list.beg->mem);
			}
			if (list.end == 0){
				printf("end     : %10i\n",list.end);
			} else {
				printf("end     : %10i next : %10i prev : %10i mem : %10i\n",list.end, list.end->next, list.end->prev, list.end->mem);
			}
			if (list.cur == 0){
				printf("cur     : %10i\n",list.cur);
			} else {
				printf("cur     : %10i next : %10i prev : %10i mem : %10i\n",list.cur, list.cur->next, list.cur->prev, list.cur->mem);
			}
			printf("pos_cur : %10i\n",list.pos_cur);
			printf("length  : %10i\n",list.length);
		}
		printf(">> ");
		scanf("%s",com);
	}
	puts("Test finished ;)");
}

#endif
