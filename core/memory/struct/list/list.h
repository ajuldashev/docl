#ifndef MEMORY_LIST
#define MEMORY_LIST
#include "../struct.h"

struct mem_list_node_st{
	struct mem_list_node_st * next;
	struct mem_list_node_st * prev;
	void * mem;
};

struct mem_list_st{
	struct mem_list_node_st * beg;
	struct mem_list_node_st * end;
	struct mem_list_node_st * cur;
	int pos_cur;
	int length;
};

void mem_list_init(struct mem_list_st * list);

void mem_list_insert(struct mem_list_st * list, void * mem, int pos);
void mem_list_push_beg(struct mem_list_st * list, void * mem); 
void mem_list_push_end(struct mem_list_st * list, void * mem);
 
void * mem_list_pop_beg(struct mem_list_st * list);
void * mem_list_pop_end(struct mem_list_st * list);

void * mem_list_at(struct mem_list_st * list, int pos);

void mem_list_next(struct mem_list_st * list);
void mem_list_prev(struct mem_list_st * list);

void mem_list_goto_beg(struct mem_list_st * list);
void mem_list_goto_end(struct mem_list_st * list);

void * mem_list_get(struct mem_list_st * list, int pos);

void mem_list_free(struct mem_list_st * list);
 
#endif

#define DEBUG
#ifdef  DEBUG

#include <stdio.h>
#include <string.h>
void mem_list_test();

#endif
