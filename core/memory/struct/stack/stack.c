#include "stack.h"

void mem_stack_init(struct mem_stack_st * stack){
	stack-> head = 0;
	stack->length = 0;
}

void mem_stack_push(struct mem_stack_st * stack, void * mem){
	if (stack == 0 || mem == 0){
		return;
	}
	struct mem_stack_node_st * node = mem_alloc(sizeof(struct mem_stack_node_st));
	if (node == 0){
		return;
	}
	node->mem = mem;
	node->next = stack->head;
	stack->head = node;
	stack->length += 1;
}

void * mem_stack_pop(struct mem_stack_st * stack){
	if (stack == 0 || stack->length == 0){
		return 0;
	}
	void * mem = stack->head->mem;
	struct mem_stack_node_st * node = stack->head;
	stack->head = stack->head->next;
	stack->length -= 1;
	mem_free(node);
	return mem;
}

void * mem_stack_at(struct mem_stack_st * stack, int pos){
	if (stack == 0 || stack->length == 0){
		return 0;
	}
	if (pos <= 0){
		return stack->head->mem;
	}
	struct mem_stack_node_st * node = stack->head;
	int idx = 0;
	for (; node->next != 0 && idx < pos; node = node->next, idx += 1);
	return node->mem;
}

void * mem_stack_get(struct mem_stack_st * stack, int pos){
	if (stack == 0 || stack->length == 0){
		return 0;
	}
	if (pos <= 0){
		struct mem_stack_node_st * node = stack->head;
		void * mem = node->mem;
		stack->length -= 1;		
		mem_free(node);
		return mem;
	}
	if (stack->length == 1){
		void * mem = stack->head->mem;
		mem_free(stack->head);
		stack->head = 0;
		stack->length = 0;
		return mem;
	}
	struct mem_stack_node_st * node = stack->head;
	int idx = 0;
	for (; node->next->next != 0 && idx < pos - 1; node = node->next, idx += 1);
	struct mem_stack_node_st * node_res = node->next;
	void * mem = node_res->mem;
	node->next = node->next->next;
	mem_free(node_res);
	stack->length -= 1;
	return mem;
}

void mem_stack_insert(struct mem_stack_st * stack, void * mem, int pos){
	if (stack == 0){
		return;
	}
	if (pos <= 0 || stack->length == 0){
		struct mem_stack_node_st * node = mem_alloc(sizeof(struct mem_stack_node_st));
		if (node == 0){
			return;
		}
		node->mem = mem;
		node->next = stack->head;
		stack->head = node;
		stack->length += 1;
		return;
	}
	if (pos > 0 && stack->length == 1){
		struct mem_stack_node_st * node = mem_alloc(sizeof(struct mem_stack_node_st));
		if (node == 0){
			return;
		}
		node->mem = mem;
		node->next = stack->head->next;
		stack->head->next = node;
		stack->length += 1;
		return;
	}
	struct mem_stack_node_st * node_1 = stack->head;
        struct mem_stack_node_st * node_2 = stack->head->next;
	int idx = 1;
        for (; node_2 != 0 && idx < pos; node_1 = node_1->next, node_2 = node_2->next, idx += 1);
        struct mem_stack_node_st * temp = mem_alloc(sizeof(struct mem_stack_node_st));
	if (temp == 0){
		return;
	}
	temp->next = node_2;
	temp->mem = mem;
	node_1->next = temp;
        stack->length += 1;
        return;
}

void mem_stack_free(struct mem_stack_st * stack){
	while (stack->length > 0){
		mem_free(mem_stack_pop(stack));
	}
}

#ifdef DEBUG

void mem_stack_test(){
	struct mem_stack_st stack;
	mem_stack_init(&stack);
	char com[1024];
	puts("This is test stack ;)");
	printf(">> ");
	scanf("%s",com);
	while (strcmp(com, "stop") != 0){
		if (strcmp(com, "help") == 0){
			puts("*)");
			puts("1) help 	-> Show this message");
			puts("2) push 	-> push in stack element");
			puts("3) pop  	-> pop from satck element and delete");
			puts("4) print 	-> print stack");
			puts("5) get 	-> get element from pos");
			puts("6) insert -> insert element on pos in stack");
			puts("7) free	-> free all stack");
			puts("8) status -> show status stack");
			puts("*)");
		}
		if (strcmp(com, "push") == 0){
			int * num = mem_alloc(sizeof(int));
			scanf("%i",num);
			mem_stack_push(&stack, num);
		}
		if (strcmp(com, "pop") == 0){
			int *num = mem_stack_pop(&stack);
			if (num != 0) {
				printf("%10i\n", *num);
				mem_free(num);
			} else {
				puts("Stack free");
			}
		}
		if (strcmp(com, "print") == 0){
			int idx = 0;
			for (; idx < stack.length; idx += 1){
				int * num = mem_stack_at(&stack,idx);
				if (num != 0){
					printf ("%10i->%10i\n", idx, *num);
				} else {
					printf("Stack node free\n");
				}
			}
		}
		if (strcmp(com, "get") == 0){
			int pos = 0;
			scanf("%i",&pos);
			int * num = mem_stack_get(&stack, pos);
			if (num != 0){
				printf ("%10i\n", *num);
				mem_free(num);
			} else {
				printf("Stack free\n");
			}
		}
		if (strcmp(com, "insert") == 0){
			int pos = 0;
			int * num = mem_alloc(sizeof(int));
			scanf("%i %i", num, &pos);
			mem_stack_insert(&stack,num,pos);
		}
		if (strcmp(com,"free") == 0){
			mem_stack_free(&stack);
		}
		if (strcmp(com, "status") == 0){
			printf("length : %i\n", stack.length);
		}
		printf(">> ");
		scanf("%s",com);
	}
	puts("Test finished ;)");
}

#endif 
