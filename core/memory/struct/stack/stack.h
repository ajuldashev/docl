#ifndef MEMORY_STACK
#define MEMORY_STACK
#include "../struct.h"

struct mem_stack_node_st{
	struct mem_stack_node_st * next;
	void * mem;
};

struct mem_stack_st{
	struct mem_stack_node_st * head;
	int length;	
};

void mem_satck_init(struct mem_stack_st * stack);

void   mem_stack_push(struct mem_stack_st * stack, void * mem);
void * mem_stack_pop (struct mem_stack_st * stack);

void * mem_stack_at(struct mem_stack_st * stack, int pos);
void * mem_stack_get(struct mem_stack_st * stack, int pos);
void   mem_stack_insert(struct mem_stack_st * stack, void * mem, int pos);

void mem_stack_free(struct mem_stack_st * stack);

#endif

#define DEBUG
#ifdef  DEBUG

#include <stdio.h>
#include <string.h>

void mem_stack_test();

#endif
