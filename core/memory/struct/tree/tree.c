#include "tree.h"

void mem_tree_init(struct mem_tree_st * tree, void * mem){
	if (mem == 0){
		tree->head = 0;
	} else {
		tree->head = mem_alloc(sizeof(struct mem_tree_node_st));
		tree->head->mem = mem;
		tree->head->right  = 0;
		tree->head->left   = 0;
		tree->head->parent = 0;
	}
	tree->curr = tree->head;
}

struct mem_tree_node_st * mem_tree_new_node(struct mem_tree_node_st * tree, void * mem){
	struct mem_tree_node_st * node = mem_alloc(sizeof(struct mem_tree_node_st));
	if (node == 0){
		return 0;
	}
	node->mem = mem;
	node->right = 0;
	node->left  = 0;
	node->parent = tree;	

	return node;
}

struct mem_tree_node_st * mem_tree_add_right(struct mem_tree_node_st * node, void * mem){
	if (node == 0 || mem == 0){
		return 0;
	}
	if (node->right == 0){
		node->right = mem_tree_new_node(node,mem);
	} else {
		struct mem_tree_node_st * new_node = mem_tree_new_node(node,mem);
		new_node->right = node->right;
		node->right = new_node;
	}
	return node->right;
}

struct mem_tree_node_st * mem_tree_add_left(struct mem_tree_node_st * node, void * mem){
	if (node == 0 || mem == 0){
		return 0;
	}
	if (node->left == 0){
		node->left = mem_tree_new_node(node,mem);
	} else {
		struct mem_tree_node_st * new_node = mem_tree_new_node(node,mem);
		new_node->left = node->left;
		node->left = new_node;
	}
	return node->left;
}

struct mem_tree_node_st * mem_tree_goto_right(struct mem_tree_st * tree){
	if (tree == 0){
		return 0;
	}
	struct mem_tree_node_st * node = tree->curr;
	if (node == 0){
		return 0;
	}
	if (node->right == 0){
		return 0;
	}
	tree->curr = node->right;
	return tree->curr;
}

struct mem_tree_node_st * mem_tree_goto_left(struct mem_tree_st * tree){
	if (tree == 0){
		return 0;
	}
	struct mem_tree_node_st * node = tree->curr;
	if (node == 0){
		return 0;
	}
	if (node->left == 0){
		return 0;
	}
	tree->curr = node->left;
	return tree->curr;
}

struct mem_tree_node_st * mem_tree_goto_parent(struct mem_tree_st * tree){
	if (tree == 0){
		return 0;
	}
	struct mem_tree_node_st * node = tree->curr;
	if (node == 0){
		return 0;
	}
	if (node->parent == 0){
		return 0;
	}
	tree->curr = node->parent;
	return tree->curr;
}

void mem_tree_free_node(struct mem_tree_node_st * node){
	if (node == 0){
		return;
	}
	if (node->right != 0){
		mem_tree_free_node(node->right);
	}
	if (node->left != 0){
		mem_tree_free_node(node->left);
	}
	mem_free(node->mem);
	mem_free(node);
}

void mem_tree_free_right(struct mem_tree_node_st * node){
	if (node == 0){
		return;
	}
	mem_tree_free_node(node->right);
}

void mem_tree_free_left(struct mem_tree_node_st * node){
	if (node == 0){
		return;
	}
	mem_tree_free_node(node->left);
}

void mem_tree_free(struct mem_tree_st * tree){
	if (tree == 0){
		return; 
	}
	mem_tree_free_node(tree->head);
	tree->head = tree->curr;
}

#ifdef DEBUG

void mem_tree_test_print(struct mem_tree_node_st * node){
	if (node == 0){
		puts("Node free");
		return;
	}
	if (node->mem == 0)
		return;{
	}
	printf("%i {", *(int*)node->mem);
	if (node->left  != 0){
		mem_tree_test_print(node->left );
	}
	printf(", ");
	if (node->right != 0){
		mem_tree_test_print(node->right);
	}
	printf("}");
}

void mem_tree_test(){
	struct mem_tree_st tree;
	mem_tree_init(&tree, 0);

	char com[1024];
	puts("Hello this is tree test ;)");
	printf(">> ");
	scanf("%s",com);
	while (strcmp(com, "stop") != 0){
		if (strcmp(com, "help") == 0){
			puts("*)");
			puts("1) help 	-> Show this message");
			puts("2) init 	-> init tree");
			puts("3) goto 	-> goto to branch the tree left/right/parent");
			puts("4) add 	-> add a new breach to left/right");
			puts("5) print 	-> print the tree");
			puts("6) free	-> free the tree");
			puts("*)");
		}
		if (strcmp(com,"init") == 0){
			int * i = mem_alloc(sizeof(int));
			scanf("%i",i);
			mem_tree_init(&tree, i);
		}
		if (strcmp(com, "goto") == 0){
			char gt[10];
			scanf("%s",gt);
			if (strcmp(gt, "right") == 0){
				struct mem_tree_node_st * node = mem_tree_goto_right(&tree);
				if (node == 0){
					puts("Right free");
				}else{
					printf("Current : %i\n", *(int *)(node->mem));
				}
			}
			if (strcmp(gt, "left") == 0){
				struct mem_tree_node_st * node = mem_tree_goto_left(&tree);
				if (node == 0){
					puts("Left free");
				}else{
					printf("Current : %i\n", *(int *)(node->mem));
				}
			}
			if (strcmp(gt, "parent") == 0){
				struct mem_tree_node_st * node = mem_tree_goto_parent(&tree);
				if (node == 0){
					puts("Parent is root");
				}else{
					printf("Current : %i\n", *(int *)(node->mem));
				}
			}
		}
		if (strcmp(com, "add") == 0){
			char add[20];
			scanf("%s",add);
			if (strcmp(add, "right") == 0){
				int* i = mem_alloc(sizeof(int));
				scanf("%i", i);
				if (tree.head == 0){
					mem_tree_init(&tree, i);
				} else {
					mem_tree_add_right(tree.curr, i);
				}
			}
			if (strcmp(add, "left") == 0){
				int* i = mem_alloc(sizeof(int));
				scanf("%i", i);
				if (tree.head == 0){
					mem_tree_init(&tree, i);
				} else {
					mem_tree_add_left(tree.curr, i);
				}
			}
		}
		if (strcmp(com, "print") == 0){
			mem_tree_test_print(tree.head);
			puts("");
		}
		if (strcmp(com, "free") == 0){
			mem_tree_free(&tree);
		}
		printf(">> ");
		scanf("%s", com);	
	}
	puts("Test finished ;)");
}

#endif
