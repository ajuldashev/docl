#ifndef MEMORY_TREE
#define MEMORY_TREE
#include "../struct.h"

struct mem_tree_node_st{
	struct mem_tree_node_st * right;
	struct mem_tree_node_st * left;
	struct mem_tree_node_st * parent;
	void * mem;	
};

struct mem_tree_st{
	struct mem_tree_node_st * head;
	struct mem_tree_node_st * curr;
};

void mem_tree_init(struct mem_tree_st * tree, void * mem);

struct mem_tree_node_st * mem_tree_new_node(struct mem_tree_node_st * tree, void * mem);

struct mem_tree_node_st * mem_tree_add_right(struct mem_tree_node_st * node, void * mem);
struct mem_tree_node_st * mem_tree_add_left (struct mem_tree_node_st * node, void * mem);

struct mem_tree_node_st * mem_tree_goto_right (struct mem_tree_st * tree);
struct mem_tree_node_st * mem_tree_goto_left  (struct mem_tree_st * tree);
struct mem_tree_node_st * mem_tree_goto_parent(struct mem_tree_st * tree);

void mem_tree_free_node(struct mem_tree_node_st * node);

void mem_tree_free_right(struct mem_tree_node_st * node);
void mem_tree_free_left (struct mem_tree_node_st * node);

void mem_tree_free(struct mem_tree_st * tree);

#endif

#define DEBUG
#ifdef  DEBUG

#include <stdio.h>
#include <string.h>
void mem_tree_test();

#endif

