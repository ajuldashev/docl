#include "memory.h"


#ifdef DEBUG

void memory_test(){
	char com[1024];
	puts("This is testing all memory module ;)");
	printf(">> ");
	scanf("%s", com);
	while (strcmp(com, "stop") != 0){
		if (strcmp(com, "help") == 0){
			puts("*)");
			puts("1) help    -> this message");
			puts("2) manager -> testing memory manager");
			puts("3) list    -> testing memory list struct");
			puts("4) stack   -> testing memory stack struct");
			puts("5) tree    -> testing memory tree struct");
			puts("*)");
		}
		if (strcmp(com, "manager") == 0){
			mem_test();
		}
		if (strcmp(com, "list") == 0){
			mem_list_test();
		}
		if (strcmp(com, "stack") == 0){
			mem_stack_test();
		}
		if (strcmp(com, "tree") == 0){
			mem_tree_test();
		}

		printf(">> ");
		scanf("%s", com);
	}
	puts("Testing finfshed ;)");
}

#endif
