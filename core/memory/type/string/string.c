#include "string.h"

string str_new(int l){
	if (l == 0){
		return 0;
	}
	string str = mem_alloc(l + 1);
	str[l] = '\0';
	return str;
}

int str_length(const string str){
	if (str == 0){
		return 0;
	}
	int len = 0;
	while (str[len]){
		len += 1;
	}
	return len;
}

string str_copy(const string str){
	int len = str_length(str);
	if (len <= 0){
		return 0;
	}
	string new_str = str_new(len);
	int i = 0;
	for (i = 0; i < len; ++i){
		new_str[i] = str[i];
	}
	new_str[i] = "\0";
	return new_str;
}

int str_cmp(const string str1, const string str2){
	if (str1 == 0){
		return - *str2;
	}
	if (str2 == 0){
		return   *str1;
	}
	int pos = 0;
	while ((str1[pos] - str2[pos]) == 0){
		pos += 1;
	}
	return str1[pos] - str2[pos];
}

string str_cat(const string str1, const string str2){
	if (str1 == 0){
		return str_copy(str2);
	}
	if (str2 == 0){
		return str_copy(str1);
	}
	int len1 = str_length(str1);
	int len2 = str_length(str2);
	string new_str = str_new(len1 + len2);
	int pos = 0;
	for (; pos < len1; ++pos){
		new_str[pos] = str1[pos];
	}
	for (; pos < len2; ++pos){
		new_str[pos] = str2[pos - len1];
	}
	return new_str;
}

int str_str(const string str1, const string str2){
	if (str1 == 0){
		return -1;
	}
	if (str2 == 0){
		return -1;
	}
	int len1 = str_length(str1);
	int len2 = str_length(str2);
	if (len1 > len2){
		return -1;
	}
	int pos = 0;
	for (; pos < len2 - len1; ++pos){
		if (str_cmp(str1, str2 + pos)){
			return pos;
		}
	}
	return -1;
}

string str_sub(const string str1, const string str2){
	int pos = str_str(str1, str2);
	if (pos == -1){
		return 0;
	}
	int len1 = str_length(str1);
	int len2 = str_length(str2);
	int i = 0;
	string new_str = str_new(len2 - len1);
	for (; i < pos; ++i){
		new_str[i] = str2[i];
	}
	for (i = len1 + pos; i < len2; ++i){
		new_str[i - pos] = str2[i];
	}
	return new_str;
}

void str_free(string str){
	mem_free(str);
}
