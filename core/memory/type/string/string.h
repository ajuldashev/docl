#ifndef MEMORY_STRING
#define MEMORY_STRING
#include "../type.h"

typedef char* string;

string str_new(int l);
int    str_length(const string str);
string str_copy(const string str);
int    str_cmp(const string str1, const string str2);

string str_cat(const string str1, const string str2);
string str_sub(const string str1, const string str2);
int    str_str(const string str1, const string str2);

long   str_to_int(string str);
double str_to_double(string str);
string int_to_str(int i);
string int_to_hex(int i);
string double_to_str(double d, const string format);

void str_free(string str);

#endif

