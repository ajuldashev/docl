//#define x32_system
//#define x64_system

#ifndef MEMORY_TYPE
#define MEMORY_TYPE
#include "../memory.h"

enum types{
	_unknown_types,
	_bool,
	_byte,
	_long_byte, 
	_short,
	_long_short,
	_int,
	_long_int,
	_long,
	_long_long,
	_char,
	_long_char,
	_string,
	_long_string,
	_byte_array,
	_table,
	_function
};

enum type_signed{
	_signed,
	_unsigned
};

enum type_size{
	_unknown_type,
	_bool_size 	 = 1,
	_byte_size 	 = 1,
	_long_byte_size  = 2,
	_short_size 	 = 2,
	_long_short_size = 4,
	_int_size	 = 4,
	_long_int_size 	 = 8,
	_long_size 	 = 8,
	_long_long_size  = 16,
	_char_size 	 = 1,
	_long_char_size  = 2,
};

struct variable_st{
	void * var;
	enum types type;
};

struct bool_st{
	unsigned char b0;
};

struct byte_st{
	unsigned char b0;
};	

struct long_byte_st{
	unsigned char b0;
	unsigned char b1;
};

struct short_st{
	unsigned char b0;
	unsigned char b1;	
};

struct long_short_st{
	unsigned char b0;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;
};

struct int_st{
	unsigned char b0;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;
};

struct long_int_st{
	unsigned char b0;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;

	unsigned char b4;
	unsigned char b5;
	unsigned char b6;
	unsigned char b7;
};

struct long_st{
	unsigned char b0;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;

	unsigned char b4;
	unsigned char b5;
	unsigned char b6;
	unsigned char b7;
};

struct long_long_st{
	unsigned char b0;
	unsigned char b1;
	unsigned char b2;
	unsigned char b3;

	unsigned char b4;
	unsigned char b5;
	unsigned char b6;
	unsigned char b7;

	unsigned char b8;
	unsigned char b9;
	unsigned char ba;
	unsigned char bb;

	unsigned char bc;
	unsigned char bd;
	unsigned char be;
	unsigned char bf;
};

struct char_st{
	unsigned char b0;
};

struct long_char_st{
	unsigned char b0;
	unsigned char b1;
};

struct string_st{
	struct char_st * str;
};

struct long_string_st{
	struct long_char_st * str;
};

struct table_st{

};

struct function_st{

};

#endif

