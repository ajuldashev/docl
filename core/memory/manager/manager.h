/*
	This is manager for alloc/free memory
*/
#ifndef MANAGER_MEMORY
#define MANAGER_MEMORY

#define MEMORY_SIZE_BLOCK (0xff*1024*1024)
#define MEMORY_QTY_REC_IN_TABLE (MEMORY_SIZE_BLOCK / (0xf))

struct mem_rec_st{
	void * mem;
	unsigned int mem_len_block;
};

void * mem_alloc(unsigned int size);
void   mem_free (void * point);

#endif

#define DEBUG
#ifdef  DEBUG

#include <stdio.h>
#include <string.h>
void mem_test();

#endif
