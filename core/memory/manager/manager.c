#include "manager.h"

static char local_mem[MEMORY_SIZE_BLOCK];

static struct mem_rec_st local_table[MEMORY_QTY_REC_IN_TABLE];

static unsigned int local_qty_rec = 0;

static void local_push(struct mem_rec_st rec){
	if (local_qty_rec == 0){
		local_table[local_qty_rec] = rec;
		local_qty_rec += 1;
		return;
	}
	if (local_qty_rec == MEMORY_QTY_REC_IN_TABLE){
		return;
	}
	int idx = 0;
	for (; idx < local_qty_rec && rec.mem > local_table[idx].mem; ++idx);
	if (idx == local_qty_rec){
		local_table[idx] = rec;
		local_qty_rec += 1;
		return;
	}
	int idx2 = local_qty_rec;
	for (; idx2 > idx; --idx2){
		local_table[idx2] = local_table[idx2 - 1];
	}
	local_table[idx] = rec;
	local_qty_rec += 1;
}	

static void local_pop(void * point){
	int idx = 0;
	for (; idx < local_qty_rec; ++idx){
		if (point == local_table[idx].mem){
			for (; idx < local_qty_rec; ++idx){
				local_table[idx] = local_table[idx + 1];
			}
			local_qty_rec -= 1;
			break;
		}
	}
}

void * mem_alloc(unsigned int size){
	if (local_qty_rec == MEMORY_QTY_REC_IN_TABLE){
		return 0;
	}	
	struct mem_rec_st rec;
	rec.mem = 0;
	rec.mem_len_block = 0;	

	if (local_qty_rec == 0 && size <= MEMORY_SIZE_BLOCK){
		rec.mem = local_mem;
		rec.mem_len_block = size;
		local_push(rec);
		return rec.mem;
	}
	
	struct mem_rec_st first_rec = local_table[0];
	unsigned int first_len = (unsigned int)((char*)first_rec.mem - local_mem);
	if (size <= first_len){
		rec.mem = first_rec.mem;
		rec.mem_len_block = size;
		local_push(rec);	
		return rec.mem;
	}

	if (local_qty_rec == 1 && size <= local_mem + MEMORY_SIZE_BLOCK -(char*)local_table[0].mem - local_table[0].mem_len_block){
		rec.mem = local_table[0].mem + local_table[0].mem_len_block;
		rec.mem_len_block = size;
		local_push(rec);
		return rec.mem;
	}
	
	struct mem_rec_st rec_a;
	struct mem_rec_st rec_b;
	
	unsigned int idx = 0;
	
	for (;idx < local_qty_rec - 1; ++idx){
		rec_a = local_table[idx];
		rec_b = local_table[idx + 1];
		unsigned int len = (char*)rec_b.mem - ((char*)rec_a.mem + rec_a.mem_len_block);
		if (size <= len){
			rec.mem = rec_a.mem + rec_a.mem_len_block;
			rec.mem_len_block = size;
			local_push(rec);
			return rec.mem;
		}
	}
	unsigned int last_len = (local_mem + MEMORY_SIZE_BLOCK) - ((char*)rec_b.mem + rec_b.mem_len_block);
	if (size <= last_len){
		rec.mem = rec_b.mem + rec_b.mem_len_block;
		rec.mem_len_block = size;
		local_push(rec);
		return rec.mem;
	}

	return 0;
}

void mem_free(void * point){
	local_pop(point);
}

#ifdef DEBUG
void mem_test(){
	puts("This in test memory/manager ;)");
	char com[1024];
	printf(">> ");
	scanf("%s",com);
	while (strcmp(com,"stop") != 0){
		if (strcmp(com,"alloc") == 0){
			printf("Print size >> ");
			unsigned int len = 0;
			scanf("%i",&len);
			printf("Alloc : %i\n", mem_alloc(len));
		}
		if (strcmp(com,"free") == 0){
			printf("Print mem >> ");
			unsigned long mem = 0;
			scanf("%i",&mem);
			mem_free(mem);
		}
		if (strcmp(com,"table") == 0){
			unsigned int idx = 0;
			for (; idx < local_qty_rec; ++idx){
				printf("%20i : %20i\n",local_table[idx].mem, local_table[idx].mem_len_block);
			}
		}
		if (strcmp(com, "help") == 0){
			puts("*)");
			puts("1) help  -> Show this is massage");
			puts("2) alloc -> alloc memory");
			puts("3) free  -> free  memory");
			puts("4) table -> print alloced memory on table");
			puts("*)");
		}
		printf(">> ");
		scanf("%s",com);
	}
	puts("Test ended!");
}
#endif
