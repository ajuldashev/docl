all: docl

docl: docl.o core.o core_type.o manager.o memory.o struct.o  stack.o list.o tree.o type.o string.o
	gcc docl.o core.o core_type.o manager.o memory.o stack.o list.o tree.o type.o string.o -o docl

core.o: core/core.c
	gcc -c core/core.c 

core_type.o: core/type/type.c
	gcc -c core/type/type.c -o core_type.o

manager.o: core/memory/manager/manager.c
	gcc -c -Wpointer-arith core/memory/manager/manager.c

memory.o: core/memory/memory.c
	gcc -c core/memory/memory.c

struct.o: core/memory/struct/struct.c
	gcc -c core/memory/struct/struct.c

list.o: core/memory/struct/list/list.c 
	gcc -c core/memory/struct/list/list.c

stack.o: core/memory/struct/stack/stack.c 
	gcc -c core/memory/struct/stack/stack.c

tree.o: core/memory/struct/tree/tree.c
	gcc -c core/memory/struct/tree/tree.c

type.o: core/memory/type/type.c
	gcc -c core/memory/type/type.c

string.o: core/memory/type/string/string.c
	gcc -c core/memory/type/string/string.c

docl.o: docl.c
	gcc -c docl.c 

clear:
	rm *.o docl
